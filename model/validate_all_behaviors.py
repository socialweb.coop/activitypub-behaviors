import os
import sys
import yaml
import pprint
from linkml_validator.validator import Validator

script_directory = os.path.dirname(os.path.abspath(sys.argv[0]))
protocol_model_path=os.path.join(script_directory, 'protocol.linkml.yaml')
validator = Validator(schema=protocol_model_path)
behaviors_dir = os.path.join(script_directory, "../behaviors")
invalid_behaviors_exist = False

for filename in os.listdir(behaviors_dir):
    if not filename.endswith(".yaml"): continue
    yaml_path = os.path.join(behaviors_dir, filename)
    loaded_yaml = yaml.safe_load(open(yaml_path, 'r'))
    validation_result = validator.validate(
        obj=loaded_yaml,
        target_class=loaded_yaml['type'],
        strict=True,
    )
    if not validation_result.valid:
        invalid_behaviors_exist = True
        print("file", yaml_path)
        for vr in validation_result.validation_results:
            for vm in vr.validation_messages:
                print(vm.message)
        print()

if invalid_behaviors_exist:
    sys.exit("some behaviors are invalid")
