# @socialweb.coop/activitypub-behaviors

## 0.5.0

### Minor Changes

- 8b48de4: all behaviors have a tag property which is an array of strings, and all behaviors have at least one tag of ActivityPubClient, ActivityPubServer
