import * as assert from 'node:assert/strict'
import test from 'node:test';

import * as it from 'streaming-iterables';
import { activityPubBehaviors, toCollection } from '../src/index.mjs';
import * as activityPubBehaviorsLib from '../src/index.mjs'
import * as index from '../src/index.mjs'

test('activityPubBehaviors', async t => {
  await test('can async iterate behaviors', async t => {
    let behaviorCount = 0;
    for await (const behavior of activityPubBehaviors) {
      behaviorCount++
      assert.equal(behavior.type, 'Behavior', `behavior.type === 'Behavior'`)
      assert.equal(Array.isArray(behavior.tag), true, `Array.isArray(behavior.tag)`)
      assert.ok(behavior.tag.find(tag => ['ActivityPubServer', 'ActivityPubClient'].includes(String(tag))), 'behavior.tag has at least one of ActivityPubServer | ActivityPubClient')
    }
    assert.ok(behaviorCount >= 50, 'there are at least 50 behaviors')
  })
  await test('can do toCollection to get as2 collection', async t => {
    const collection = toCollection(await it.collect(activityPubBehaviors))
    const collectionTypes = Array.isArray(collection.type) ? collection.type : [collection.type]
    assert.ok(collectionTypes.includes('Collection'))
    assert.deepEqual(collection.license, activityPubBehaviorsLib.license)
  })
})

test('activityPubRequirements', async t => {
  await test('all requirements have a MUST', async t => {
    for await (const requirement of index.activityPubRequirements) {
      assert.ok(JSON.stringify(requirement).match('MUST'), 'requirement contains MUST')
    }
  })
  await test('can iterate it twice', async t => {
    const iteration1 = await it.collect(index.activityPubRequirements)
    assert.ok(iteration1.length >= 1, 'at least one requirement')
    const iteration2 = await it.collect(index.activityPubRequirements)
    assert.equal(iteration1.length, iteration2.length)
  })
})
