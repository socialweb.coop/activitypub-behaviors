import * as assert from 'node:assert/strict'
import * as fs from 'node:fs/promises'
import * as path from 'node:path'
import test from 'node:test';
import * as it from 'streaming-iterables'
import * as url from 'url';
import * as yaml from "yaml"

const __filename = url.fileURLToPath(import.meta.url);
const __dirname = url.fileURLToPath(new URL('.', import.meta.url));

const DEFAULT_REQUIREMENTS_DIR = `${__dirname}/../behaviors`

test('requirement yamls', async t => {
  await t.test('there are at least 50', async t => {
    const collectedFiles = await it.collect(iterateRequirementYamls(DEFAULT_REQUIREMENTS_DIR))
    assert.ok(collectedFiles.length >= 50, 'there are at least 50 requirement yaml files')
  })
  for await (const yamlEntry of iterateRequirementYamls(DEFAULT_REQUIREMENTS_DIR)) {
    const yamlPath = path.join(DEFAULT_REQUIREMENTS_DIR, yamlEntry.name)
    const fileContents = await fs.readFile(yamlPath, 'utf8')
    /** @type {any} */
    let parsed
    await t.test(`${yamlEntry.name} can be parsed as yaml`, async () => {
      parsed = yaml.parse(fileContents)
    })
    await t.test(`${yamlEntry.name} .type includes Behavior`, () => {
      const parsedTypes = Array.isArray(parsed.type) ? parsed.type : parsed.type ? [parsed.type] : undefined
      assert.ok(parsedTypes.includes('Behavior'), 'parsed yaml `type` property includes "Behavior"')
    })
    await t.test(`${yamlEntry.name} .tag includes at least one of ActivityPubServer, ActivityPubClient`, () => {
      const parsedTags = Array.isArray(parsed.tag) ? parsed.tag : parsed.tag ? [parsed.tag] : undefined
      assert.ok(parsedTags.includes('ActivityPubServer') | parsedTags.includes('ActivityPubClient'), 'parsedTags has an expected value')
    })
  }
})

/**
 * @param {string} dirpath - path to directory of requirements files
 */
function iterateRequirementYamls(dirpath) {
  const yamlEntries = it.pipeline(
    () => walk(dirpath),
    it.filter(dirent => {
      const isYaml = dirent.isFile() && dirent.name.endsWith('.yaml')
      return isYaml
    })
  )
  return yamlEntries
}

/**
 * @param {string} dirpath
 * @returns {AsyncIterable<import('fs').Dirent>}
 */
async function* walk(dirpath) {
  for await (const d of await fs.opendir(dirpath)) {
      const entryPath = path.join(dirpath, d.name);
      if (d.isDirectory()) yield* walk(entryPath);
      else if (d.isFile()) yield d
  }
}
