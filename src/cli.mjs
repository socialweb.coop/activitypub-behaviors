#!/usr/bin/env node

import * as url from 'url';
import * as fs from 'fs';
import { activityPubBehaviors } from '../src/index.mjs';
import dedent from "dedent";
import { parseArgs } from "node:util";
import { Readable } from 'stream';

const helpText = dedent`
  Print protocol behaviors described in the W3C ActivityPub[0] specification as JSON to stdout.

  Browse on them on the web at https://socialweb.coop/activitypub/behaviors/.

  Usage:
    activitypub-behaviors [-h]

  Flags:
    -h  show help text

  [0]: https://www.w3.org/TR/activitypub/
`

/**
 * activitypub-behaviors cli functionality
 * @param  {...any} argv - process.argv
 */
async function cli(...argv) {
  const args = argv.slice(2)
  const {
    values: { help },
  } = parseArgs({
    args,
    options: {
      help: {
        type: "boolean",
        short: "h",
      }
    },
  });
  if (help) {
    return console.log(helpText)
  }
  Readable.from(async function * () {
    for await (const behavior of activityPubBehaviors) {
      yield JSON.stringify(behavior) + '\n'
    }
  }()).pipe(process.stdout).on('error', (error) => {
    switch (error?.code) {
      case "EPIPE":
        console.warn('EPIPE')
        // normal
        break;
      default:
        throw error
    }
  })
}

export default cli

// run as cli if this script is executed directly
if (process.argv[1] === url.fileURLToPath(import.meta.url)) {
  await cli(...process.argv)
}
