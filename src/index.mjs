import * as url from 'url';
import * as path from 'path';
import * as fs from 'fs';
import * as yaml from "yaml"

/**
 * @typedef {import('node:fs').Dir} Directory
 */

/**
 * @typedef ProtocolBehavior
 * @property {"Behavior"} type - https://www.w3.org/TR/activitystreams-vocabulary/#dfn-type
 * @property {Array<unknown>} tag - https://www.w3.org/TR/activitystreams-vocabulary/#dfn-tag
 */

const __dirname = url.fileURLToPath(new URL('.', import.meta.url));

export class YamlDirectoryObjectIterator {
  /** @type {() => Directory} */
  #directory

  /**
   * 
   * @param {() => Directory} dir - directory of yaml files
   */
  constructor(dir) {
    this.#directory = dir
  }

  /**
   * @returns {AsyncIterableIterator<ProtocolBehavior>}
   */
  async *[Symbol.asyncIterator]() {
    try {
      const dir = this.#directory()
      for await (const entry of dir) {
        if ( ! this.isYamlName(entry.name)) {
          continue;
        }
        const entryPath = path.join(dir.path, entry.name)
        const entryContents = fs.readFileSync(entryPath, 'utf8');
        const entryParsedYaml = yaml.parse(entryContents)
        yield toProtocolBehavior(entryParsedYaml)
      }
    } catch (error) {
      switch (/** @type {any} */ (error).code) {
        default:
          throw error;
      }
    } finally {
    }
  }

  /**
   * @param {string} name - file name that maybe ends with .yaml
   * @returns whether or not the name should be treated as a parseable yaml file
   */
  isYamlName(name) {
    return name.endsWith('.yaml')
  }
}

const behaviorsDirectoryPath = path.join(__dirname, '../behaviors')

/**
 * all documented behaviors (of implementations) described in the ActivityPub specification
 */
export const activityPubBehaviors = new YamlDirectoryObjectIterator(() => fs.opendirSync(behaviorsDirectoryPath))

/**
 * only the behaviors that are requirements per RFC2119 (i.e. 'MUST')
 */
export const activityPubRequirements = activityPubBehaviors

export const license = ['http://gnu.org/licenses/agpl-3.0.html']

/**
 * @param {Array<ProtocolBehavior>} items
 */
export function toCollection(items, _license=license) {
  // http://purl.org/dc/terms/license
  let license = undefined
  const agplv3 = 'http://gnu.org/licenses/agpl-3.0.html'
  return {
    type: ["Collection"],
    items: [...items],
    license: _license,
    "@context": [
      "https://www.w3.org/ns/activitystreams",
      { license: 'http://purl.org/dc/terms/license' },
    ]
  }
}

/**
 * @param {any} input
 * @returns {ProtocolBehavior}
 */
function toProtocolBehavior(input) {
  if (input?.type !== 'Behavior') {
    throw Object.assign(
      new Error('unable to convert input to ProtocolBehavior'),
      { input, foo: 'bar' }
    )
  }
  if ( ! input.uuid) {
    throw new Error(`expected behavior to have a uuid, but its falsy`, { cause: input })
  }
  const id = input['@id'] || input['id'] || `urn:uuid:${input.uuid}`
  const jsonLdContext = input["@context"] || [
    "https://www.w3.org/ns/activitystreams",
    "https://socialweb.coop/ns/testing/context.json"
  ];
  return {
    id,
    ...input,
    "@context": jsonLdContext,
  };
}
