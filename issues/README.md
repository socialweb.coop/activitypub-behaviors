# activitypub-behaviors/issues

This directory is the issue tracker for `activitypub-behaviors`.

If you are having an issue using activitypub-behaviors, or you have an issue with the current set of functionality and would like it to be different, you are welcome to write a description of your issue as a file in this directory and commit it using [git](https://en.wikipedia.org/wiki/Git).

## Where to make Pull Requests

If you'd like your issue to be merged into the git repository of the activitypub-behaviors repository used by the creators of activitypub-behaviors, e.g. if you would like to bring it to their attention or request help in resolving your issue, make a pull request.

The best place to send a pull request to the activitypub-behaviors community is via the [codeberg.org/socialweb.coop/activitypub-behaviors][activitypub-behaviors-codeberg] repository.

[activitypub-behaviors-codeberg]: https://codeberg.org/socialweb.coop/activitypub-behaviors

[Click this link to add a new issue](https://codeberg.org/socialweb.coop/activitypub-behaviors/_new/main/issues) using the codeberg.org Web UI. [Markdown](https://en.wikipedia.org/wiki/Markdown) is a good syntax to use to describe your issue.

## Bugs

If you're filing a bug, try to include the following:
* what you did that led to the bug
* how the maintainers can try to reproduce the bug (e.g. sample inputs or invocations)
* what you expected to happen
* what actually happened

Why?
1. To fix the bug, and espeically to verify that it has been fixed after a change, the maintainers will probably need your help reproducing the bug or at least approximating it.
2. Discussing the gap between expected behavior and actual behavior can help get to the core of your issue as fast as possible and help the maintainers to both set realistic expectations and also improve the tool in the future to meet the expectations you already had.

## Why are you tracking issues in files and not a web based issue tracker?

1. to avoid reliance on any one issue tracking platform
1. if you have the code, you have the issues, even if you're not connected to the internet for a bit
3. so issue lifecycles can be updated, e.g. marked as fixed, at the same time as the code that fixes them.
