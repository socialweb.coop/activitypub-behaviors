# activitypub-behaviors

a dataset of behaviors[^1] described by the [ActivityPub](https://www.w3.org/TR/activitypub/) protocol.

[^1]: 'behavior' is used in the sense of [RFC2119](https://datatracker.ietf.org/doc/html/rfc2119), which is cited by ActivityPub to define its requirements, recommendations, prohibitions, etc.

## Have an Issue?

See [./issues/README.md](./issues/README.md) (tl;dr PR your issue as a file in that directory).

## Usage

### Use as a command line tool

If you have `npm` installed, you can run this command to print all the behaviors as newline-delimited JSON to your console:

```shell
npx @socialweb.coop/activitypub-behaviors
```

If you want, you can pipe that [ndjson](https://ndjson.org/) to a tool like [jq](https://jqlang.github.io/jq/) to wrangle the JSON, e.g. this shows the `uuid` and `content` of all the behaviors mentioning the `bcc` property that contain a `MUST` requirement level:

```shell
⚡  npx @socialweb.coop/activitypub-behaviors | jq '. | select(.content | test("bcc")) | select(.content | test("MUST")) | {uuid,content}'
{
  "uuid": "7f16b1b5-6c95-4d4c-83f1-3a3962a2e37c",
  "content": "When objects are received in the outbox..., the server MUST target and deliver to: The to, bto, cc, bcc or audience fields if their values are individuals or Collections owned by the actor.\n"
}
{
  "uuid": "e69ce2d6-2946-4513-b475-10690767a734",
  "content": "The server MUST remove the bto and/or bcc properties, if they exist, from the ActivityStreams object before delivery\n"
}
{
  "uuid": "f3725db1-1e6b-4d20-9cea-a49ef4291e9f",
  "content": "Any to, bto, cc, bcc, and audience properties specified on the object MUST be copied over to the new Create activity by the server.\n"
}
{
  "uuid": "08ed852d-f36c-47ba-992b-b211e35dbdca",
  "content": "Origin servers sending publicly addressed activities to sharedInbox endpoints MUST still deliver to actors and collections otherwise addressed (through to, bto, cc, bcc, and audience) which do not have a sharedInbox and would not otherwise receive the activity through the sharedInbox mechanism.\n"
}
{
  "uuid": "24dc401b-1d15-4232-b730-a31dd5e6c4a0",
  "content": "The server... MUST utilize the addressing originally stored on the bto / bcc properties for determining recipients in delivery.\n"
}
```

You can also install `activitypub-behaviors` globally so it is available on your PATH for repeat usage

```shell
npm install -g @socialweb.coop/activitypub-behaviors
# then `activitypub-behaviors` should print ndjson to stdout
```

Example

```shell
⚡  activitypub-behaviors | head -n1
{"id":"urn:uuid:fc2bfa75-8fd0-45e0-809d-7aa3970be8ae","type":"Behavior","name":"intransient objects must have unique global identifiers","content":"all objects distributed by the ActivityPub protocol MUST have unique global identifiers, unless they are intentionally transient\n","origin":{"source":"https://www.w3.org/TR/activitypub/","section":{"id":"https://www.w3.org/TR/activitypub/#obj-id","branch":[3,1]},"selector":{"type":"TextQuoteSelector","exact":"all objects distributed by the ActivityPub protocol MUST have unique global identifiers, unless they are intentionally transient (short lived activities that are not intended to be able to be looked up, such as some kinds of chat messages or game notifications).\n","prefix":"All Objects in [ActivityStreams] should have unique global identifiers. ActivityPub extends this requirement; \n","suffix":"These identifiers must fall into one of the following groups:\n"}},"uuid":"fc2bfa75-8fd0-45e0-809d-7aa3970be8ae","@context":["https://www.w3.org/ns/activitystreams","https://socialweb.coop/ns/testing/context.json"]}
EPIPE
```

### Use as a JavaScript lib

You can use this package in your project by installing it with npm

```shell
npm install @socialweb.coop/activitypub-behaviors
```

Iterate all the behaviors
```js
import { activityPubBehaviors } from '@socialweb.coop/activitypub-behaviors'
for await (const behavior of activityPubBehaviors) {
  console.log('ActivityPub behavior', behavior)
}
```

Iterate all the required behaviors (aka requirements)
```js
import { activityPubRequirements } from '@socialweb.coop/activitypub-behaviors'
for await (const behavior of activityPubRequirements) {
  console.log('ActivityPub required behavior', behavior)
}
```

## The Shape of Behavior

Each behavior corresponds to a file like `./behaviors/*.yaml`. The syntax of the contents of thosre files is [yaml](https://en.wikipedia.org/wiki/YAML).

<dl>
  <dt>
    <a href="https://www.w3.org/TR/activitystreams-vocabulary/#dfn-type">
      type
    </a>
  </dt>
    <dd>This is always <code>Behavior</code></dd>

  <dt>uuid</dt>
    <dd>A <a href="https://en.wikipedia.org/wiki/Universally_unique_identifier">uuid</a> uniquely identifying the behavior.

  <dt>
    <a href="https://www.w3.org/TR/activitystreams-vocabulary/#dfn-name">
      name
    </a>
  </dt>
    <dd>a succinct name for the behavior suitable for appearing in lists</dd>

  <dt>
    <a href="https://www.w3.org/TR/activitystreams-vocabulary/#dfn-type">
      content
    </a>
  </dt>
    <dd>a human-readable description of the behavior as HTML</dd>

  <dt>
      <a href="https://www.w3.org/TR/activitystreams-vocabulary/#dfn-context">
        context
      </a>
  </dt>
    <dd>
      <blockquote>The notion of "context" used is intentionally vague. The intended function is to serve as a means of grouping objects and activities that share a common originating context or purpose.</blockquote>
      <cite><a href="https://www.w3.org/TR/activitystreams-vocabulary/#dfn-context">"ActivityStreams Vocabulary"@w3.org</a></cite>
      <p>
        In this dataset, we have used `context` to link to which part of the protocol the behavior is expected to appear in. It's pretty free form. But this should probably be more structured over time, by e.g. enumerating the 'behavioral contexts' (handwaves) of the protocol.
      </p>
    </dd>

  <dt>
    origin
  </dt>
    <dd>
      <p>This is the 'origin' of the behavior. i.e. why it is in this dataset. where it originated. In practice, this is almost always a <a href="https://www.w3.org/TR/annotation-model/#specific-resources">SpecificResource</a> whose <a href="https://www.w3.org/TR/annotation-model/#model-15">source</a> is <a href="https://www.w3.org/TR/activitypub/">https://www.w3.org/TR/activitypub/</a> and selecting the part(s) of the spec that defines the behavior using a <a href="https://www.w3.org/TR/annotation-model/#text-quote-selector">TextQuoteSelector</a></p>
      <p>
        It might be good to find a way of using <code><a href="https://www.w3.org/TR/activitystreams-vocabulary/#dfn-origin">as2:origin</a></code>, which is defined with a domain of Activity.
      </p>
    </dd>

  <dt>
    seeAlso
  </dt>
    <dd>
    other related resources
    </dd>

</dl>

## Usage

### with `jc`

[jc](https://kellyjonbrazil.github.io/jc/) can be useful for parsing the yamls. For example, this unix command lets you view a random requirement using [`jq`](https://jqlang.github.io/jq/).

```
⚡  ls behaviors/*.yaml | shuf | xargs cat | jc --yaml | jq '[.[] | { uuid, content, origin: .origin | {source,selector} }][:1]'
[
  {
    "uuid": "5e781a3a-4314-4ec6-b973-717456dc5da5",
    "content": "The server MUST only target the values of to, cc, and/or audience on the original object being forwarded, and not pick up any new addressees whilst recursing through the linked objects (in case these addressees were purposefully amended by or via the client).\n",
    "origin": {
      "source": "https://www.w3.org/TR/activitypub/",
      "selector": {
        "prefix": "The server SHOULD recurse through these values to look for linked objects owned by the server, and SHOULD set a maximum limit for recursion (ie. the point at which the thread is so deep the recipients followers may not mind if they are no longer getting updates that don't directly involve the recipient).\n",
        "exact": "The server MUST only target the values of to, cc, and/or audience on the original object being forwarded, and not pick up any new addressees whilst recursing through the linked objects (in case these addressees were purposefully amended by or via the client).\n",
        "suffix": "The server MAY filter its delivery targets according to implementation-specific rules (for example, spam filtering).\n"
      }
    }
  }
]
```

## Developing

Each behavior corresponds to an entry in `./behaviors` whose file name is like `{uuid}.yaml`.

### Testing

Run the [linkml](https://linkml.io/) validation with `./scripts/linkml-validate-behaviors`. No output means no errors. linkml is only used at validation time.

Run the node.js tests with `npm test`.
